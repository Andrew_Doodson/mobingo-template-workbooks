@echo off
SETLOCAL EnableDelayedExpansion
::Get login information from the user
set /p tablogin="Enter Tableau Online Login Email: " 
set /p tabpass="Enter Tableau Online Login Password: "
set /p dbpass="Enter DB Password: "

::Get the mobingo site that is being published
set /p sitename="Enter Tableau Name for Mobingo Site: "
::Set the srpitpath to be equal to the folder that the batch file is running in.
set scriptpath=%~dp0

::Login to the site
tabcmd login -s https://10ay.online.tableau.com/#/site/cashed -u %tablogin% -p %tabpass%

::Publish the marketing reports to the INTERNAL MARKETING area

::Wins
::Commented Out Because this takes too long to publish
::tabcmd publish "%scriptpath%Big Wins - Mobingo.twb" -o --db-password !dbpass! -save-db-password --parent-project-path "New Structure/!sitename!/!sitename! Internal/Marketing" -r "Wins" --tabbed

::Events
tabcmd publish "%scriptpath%Credit Amount Leaderboard - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Internal/!sitename! Marketing" -r "!sitename! Events" --tabbed

::Promotions
tabcmd publish "%scriptpath%Player Nicknames - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Internal/!sitename! Marketing" -r "!sitename! Promotions" --tabbed
tabcmd publish "%scriptpath%Promo Code Report - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Internal/!sitename! Marketing" -r "!sitename! Promotions" --tabbed
tabcmd publish "%scriptpath%Bonus Points Given Away - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Internal/!sitename! Marketing" -r "!sitename! Promotions" --tabbed


::Publish the Players reports to the INTERNAL PLAYERS area
::Risk
tabcmd publish "%scriptpath%Ekata - Account Verification Stats - Mobingo.twb" -o --db-password !dbpass! -save-db-password --parent-project-path "New Structure/!sitename!/!sitename! Internal/!sitename! Players" -r "!sitename! Risk" --tabbed
tabcmd publish "%scriptpath%Disabled Accounts - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Internal/!sitename! Players" -r "!sitename! Risk" --tabbed

::Funnel
tabcmd publish "%scriptpath%Onboarding - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Internal/!sitename! Players" -r "!sitename! Funnel" --tabbed

::Valuation
tabcmd publish "%scriptpath%Daily Stats On Players - Mobingo.twb" -o --db-password !dbpass! -save-db-password --parent-project-path "New Structure/!sitename!/!sitename! Internal/!sitename! Players" -r "!sitename! Valuation" --tabbed
tabcmd publish "%scriptpath%Game Performance - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Internal/!sitename! Players" -r "!sitename! Valuation" --tabbed
tabcmd publish "%scriptpath%Player LTV - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Internal/!sitename! Players" -r "!sitename! Valuation" --tabbed
tabcmd publish "%scriptpath%VIP Users - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Internal/!sitename! Players" -r "!sitename! Valuation" --tabbed
tabcmd publish "%scriptpath%Users to Target for Points - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Internal/!sitename! Players" -r "!sitename! Valuation" --tabbed
tabcmd publish "%scriptpath%Outstanding Credit Bets - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Internal/!sitename! Players" -r "!sitename! Valuation" --tabbed


::Internal Financials Area
::KPIs
tabcmd publish "%scriptpath%KPI - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Internal/!sitename! Financials" -r "!sitename! KPI" --tabbed
tabcmd publish "%scriptpath%Additional KPI Metrics - Mobingo.twb" -o --db-password !dbpass! -save-db-password --parent-project-path "New Structure/!sitename!/!sitename! Internal/!sitename! Financials" -r "!sitename! KPI" --tabbed

::Publish To Internal Games 
::tabcmd publish "%scriptpath%Game Analysis - Mobingo.twb" -o  --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Internal" -r "Games" --tabbed
tabcmd publish "%scriptpath%Search for Specific Result - Mobingo.twb" -o --db-password !dbpass! -save-db-password --parent-project-path "New Structure/!sitename!/!sitename! Internal" -r "!sitename! Games" --tabbed
tabcmd publish "%scriptpath%User Balance Over Time - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Internal" -r "!sitename! Games" --tabbed
tabcmd publish "%scriptpath%Daily Stats - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Internal" -r "!sitename! Games" --tabbed
tabcmd publish "%scriptpath%Jackpots - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Internal" -r "!sitename! Games" --tabbed


::Publish reports to the CLIENT FINANCIALS Folder

::Reconciliation
tabcmd publish "%scriptpath%Billing Report - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Client/!sitename! Financials" -r "!sitename! Reconciliation" --tabbed
tabcmd publish "%scriptpath%Transactions Report - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Client/!sitename! Financials" -r "!sitename! Reconciliation" --tabbed
tabcmd publish "%scriptpath%Daily Redemptions - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Client/!sitename! Financials" -r "!sitename! Reconciliation" --tabbed
tabcmd publish "%scriptpath%Daily Donations By Player - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Client/!sitename! Financials" -r "!sitename! Reconciliation" --tabbed

::KPI
tabcmd publish "%scriptpath%Client KPI - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Client/!sitename! Financials" -r "!sitename! KPI" --tabbed


::The comp calculations doesnt work because it uses a local excel file as well.
::tabcmd publish "%scriptpath%Comp Calculations - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/Working" -r "!sitename! - Working - Live" --tabbed

pause

