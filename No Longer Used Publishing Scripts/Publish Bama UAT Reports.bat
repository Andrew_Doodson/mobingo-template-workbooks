@echo off
SETLOCAL EnableDelayedExpansion

set /p tablogin="Enter Tableau Online Login Email: " 
set /p tabpass="Enter Tableau Online Login Password: "
set /p dbpass="Enter DB Password: "

set scriptpath=%~dp0

tabcmd login -s https://10ay.online.tableau.com/#/site/cashed -u %tablogin% -p %tabpass%

tabcmd publish "%scriptpath%Transactions Report - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Bama Bingo/BB - Working" -r "UAT" --tabbed
tabcmd publish "%scriptpath%Big Wins - Mobingo.twb" -o --db-password !dbpass! -save-db-password --parent-project-path "New Structure/Bama Bingo/BB - Working" -r "UAT" --tabbed
tabcmd publish "%scriptpath%Billing Report - Mobingo.twb" -o --db-password !dbpass! -save-db-password --parent-project-path "New Structure/Bama Bingo/BB - Working" -r "UAT" --tabbed
tabcmd publish "%scriptpath%Daily Stats On Players - Mobingo.twb" -o --db-password !dbpass! -save-db-password --parent-project-path "New Structure/Bama Bingo/BB - Working" -r "UAT" --tabbed
tabcmd publish "%scriptpath%Game Analysis - Mobingo.twb" -o  --db-password !dbpass! -save-db-password --parent-project-path "New Structure/Bama Bingo/BB - Working" -r "UAT" --tabbed
tabcmd publish "%scriptpath%Game Performance - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Bama Bingo/BB - Working" -r "UAT" --tabbed
tabcmd publish "%scriptpath%Player LTV - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Bama Bingo/BB - Working" -r "UAT" --tabbed
tabcmd publish "%scriptpath%Promo Code Report - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Bama Bingo/BB - Working" -r "UAT" --tabbed


tabcmd publish "%scriptpath%Ekata - Account Verification Stats - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Bama Bingo/BB - Working" -r "UAT" --tabbed
tabcmd publish "%scriptpath%KPI - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Bama Bingo/BB - Working" -r "UAT" --tabbed
tabcmd publish "%scriptpath%Onboarding - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Bama Bingo/BB - Working" -r "UAT" --tabbed
tabcmd publish "%scriptpath%Player Nicknames - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Bama Bingo/BB - Working" -r "UAT" --tabbed
tabcmd publish "%scriptpath%Client KPI - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Bama Bingo/BB - Working" -r "UAT" --tabbed

pause