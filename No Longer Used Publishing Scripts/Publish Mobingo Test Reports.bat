@echo off
SETLOCAL EnableDelayedExpansion

set /p tablogin="Enter Tableau Online Login Email: " 
set /p tabpass="Enter Tableau Online Login Password: "
set /p dbpass="Enter DB Password: "

set scriptpath=%~dp0

tabcmd login -s https://10ay.online.tableau.com/#/site/cashed -u %tablogin% -p %tabpass%

tabcmd publish "%scriptpath%Auth-PP Transactions Report - Test - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Mobingo/ Working" -r "Test" --tabbed
tabcmd publish "%scriptpath%Big Wins - Test - Mobingo.twb" -o --db-password !dbpass! -save-db-password --parent-project-path "New Structure/Mobingo/ Working" -r "Test" --tabbed
tabcmd publish "%scriptpath%Billing Report - Test - Mobingo.twb" -o --db-password !dbpass! -save-db-password --parent-project-path "New Structure/Mobingo/ Working" -r "Test" --tabbed
tabcmd publish "%scriptpath%Daily Stats On Players - Test - Mobingo.twb" -o --db-password !dbpass! -save-db-password --parent-project-path "New Structure/Mobingo/ Working" -r "Test" --tabbed
tabcmd publish "%scriptpath%Game Analysis (Mikayla) - Test - Mobingo.twb" -o  --db-password !dbpass! -save-db-password --parent-project-path "New Structure/Mobingo/ Working" -r "Test" --tabbed
tabcmd publish "%scriptpath%Game Performance - Test - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Mobingo/ Working" -r "Test" --tabbed
tabcmd publish "%scriptpath%Player LTV - Test - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Mobingo/ Working" -r "Test" --tabbed
tabcmd publish "%scriptpath%Promo Code Report - Test - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Mobingo/ Working" -r "Test" --tabbed


tabcmd publish "%scriptpath%Ekata - Account Verification Stats - Test - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Mobingo/ Working" -r "Test" --tabbed
tabcmd publish "%scriptpath%KPI - Test - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Mobingo/ Working" -r "Test" --tabbed
tabcmd publish "%scriptpath%Onboarding - Test - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Mobingo/ Working" -r "Test" --tabbed
tabcmd publish "%scriptpath%Player Nicknames - Test - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Mobingo/ Working" -r "Test" --tabbed
tabcmd publish "%scriptpath%Client KPI - Test - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Mobingo/ Working" -r "Test" --tabbed

pause