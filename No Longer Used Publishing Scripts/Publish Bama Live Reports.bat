@echo off
SETLOCAL EnableDelayedExpansion

set /p tablogin="Enter Tableau Online Login Email: " 
set /p tabpass="Enter Tableau Online Login Password: "
set /p dbpass="Enter DB Password: "

set scriptpath=%~dp0

tabcmd login -s https://10ay.online.tableau.com/#/site/cashed -u %tablogin% -p %tabpass%

tabcmd publish "%scriptpath%Big Wins - Mobingo.twb" -o --db-password !dbpass! -save-db-password --parent-project-path "New Structure/Bama Bingo/BB - Working" -r "Live" --tabbed
tabcmd publish "%scriptpath%Billing Report - Mobingo.twb" -o --db-password !dbpass! -save-db-password --parent-project-path "New Structure/Bama Bingo/BB - Working" -r "Live" --tabbed
tabcmd publish "%scriptpath%Client KPI - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Bama Bingo/BB - Working" -r "Live" --tabbed
tabcmd publish "%scriptpath%Credit Amount Leaderboard - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Bama Bingo/BB - Working" -r "Live" --tabbed

tabcmd publish "%scriptpath%Daily Stats On Players - Mobingo.twb" -o --db-password !dbpass! -save-db-password --parent-project-path "New Structure/Bama Bingo/BB - Working" -r "Live" --tabbed
tabcmd publish "%scriptpath%Ekata - Account Verification Stats - Mobingo.twb" -o --db-password !dbpass! -save-db-password --parent-project-path "New Structure/Bama Bingo/BB - Working" -r "Live" --tabbed
tabcmd publish "%scriptpath%Game Analysis - Mobingo.twb" -o  --db-password !dbpass! -save-db-password --parent-project-path "New Structure/Bama Bingo/BB - Working" -r "Live" --tabbed
tabcmd publish "%scriptpath%Game Performance - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Bama Bingo/BB - Working" -r "Live" --tabbed
tabcmd publish "%scriptpath%Jackpots - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Bama Bingo/BB - Working" -r "Live" --tabbed
tabcmd publish "%scriptpath%KPI - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Bama Bingo/BB - Working" -r "Live" --tabbed
tabcmd publish "%scriptpath%Onboarding - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Bama Bingo/BB - Working" -r "Live" --tabbed
tabcmd publish "%scriptpath%Player LTV - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Bama Bingo/BB - Working" -r "Live" --tabbed
tabcmd publish "%scriptpath%Player Nicknames - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Bama Bingo/BB - Working" -r "Live" --tabbed
tabcmd publish "%scriptpath%Promo Code Report - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Bama Bingo/BB - Working" -r "Live" --tabbed
tabcmd publish "%scriptpath%Search for Specific Result - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Bama Bingo/BB - Working" -r "Live" --tabbed
tabcmd publish "%scriptpath%Transactions Report - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Bama Bingo/BB - Working" -r "Live" --tabbed

tabcmd publish "%scriptpath%User Balance Over Time - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Bama Bingo/BB - Working" -r "Live" --tabbed
tabcmd publish "%scriptpath%Users to Target for Points - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Bama Bingo/BB - Working" -r "Live" --tabbed
tabcmd publish "%scriptpath%VIP Users - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Bama Bingo/BB - Working" -r "Live" --tabbed

tabcmd publish "%scriptpath%Additional KPI Metrics - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Bama Bingo/BB - Working" -r "Live" --tabbed
tabcmd publish "%scriptpath%Daily Redemptions - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Bama Bingo/BB - Working" -r "Live" --tabbed
tabcmd publish "%scriptpath%Comp Calculations - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Bama Bingo/BB - Working" -r "Live" --tabbed
tabcmd publish "%scriptpath%Daily Stats - Mobingo.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Bama Bingo/BB - Working" -r "Live" --tabbed

pause