# -*- coding: utf-8 -*-
"""
Created on Wed Dec  8 14:25:39 2021

@author: Ruby
This script will convert all files in the folder to the Bama UAT connection details.

"""
#Relevant imports required for this task. OS for reading all files in a directory, Workbook to interact with Tableau workbooks in Python
import os
from tableaudocumentapi import Workbook 

#This is the folder on the local computer that the reports are stored in
rootdir =  os.path.dirname(__file__)

#Going to loop through all of the files in this folder and make a filepath for them
for subdir, dirs, files in os.walk(rootdir):
    for file in files:
        #print os.path.join(subdir, file)
        filepath = subdir + os.sep + file
        
        #If the filepath ends in twb its a tableau workbook and we want to edit the datasource connections within it
        if filepath.endswith(".twb"):
            sourceWB = Workbook(filepath)
            print(filepath)
            #The workbook object has x amount of datasources, we want to change the connection info 
            #on all of the datasource objects that are actually datasources (not parameters)
            datasources = [ds for ds in sourceWB.datasources]
            for ds in datasources:
            #Check that the datasource has connection information (parameters do not have connection info)
                if ds.connections != []:
                #if there is connection information, change it to the desired string(s)    
                    for conn in ds.connections:
                        conn.server = "bamabingo.ceefh9tctdpa.us-east-1.rds.amazonaws.com"
                        conn.dbname = 'bamabingo-uat'
                        conn.username = "reporting"
                        print (conn.server, conn.dbname, conn.username)
            #Save the workbook
            sourceWB.save()
            
            